/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mashinaphamacyinformationsystem;

/**
 *
 * @author Victor Omondi
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.event.*;
import java.io.*;
import java.util.*;
public class EnterData implements ActionListener,MenuListener{
    //Components SetUP
    JFrame frameED,
            frameSD;
    JPanel pnlNORTH,
            pnlWEST,
            pnlCENTER, 
            pnlEAST,
            pnlSOUTH,
            pnlHome,
            pnlEnterData,
            pnlSearchData;
    JMenuBar mnuBar = new JMenuBar();
    JMenu mnuHome, 
            mnuEdit,
            mnuEnterData, 
            mnuSearchData,
            mnuDeleteData, 
            mnuHelp;
    JLabel lblMedicineName,
            lblStore,
            lblShelf,
            lblSearchByMedicineName,
            lblSearchByStore,
            lblSearchByShelf;
    JTextField txtMedicineName,
            txtSearchByMedicineName,
            txtSearchByStore,
            txtSearchByShelf;
    JTextArea txtaOutputMessage;
    JButton btnSave,
            btnCancel,
            btnSearchByMedicineName,
            btnSearchByStore,
            btnSearchByShelf;
    JComboBox cboStore,
            cboShelf,
            cboSearchByStore,
            cboSearchByShelf;
    JTextArea txtHome = new JTextArea();

    
    //Data Setup
    ArrayList<String> alMedicineName = new ArrayList<String>();
    ArrayList<String> alStore = new ArrayList<String>();
    ArrayList<String> alShelf = new ArrayList<String>();
    public EnterData(){
        //Setting Frames
        frameED = new JFrame("Enter Data | Mashina Pharmacy Information System");
        frameSD = new JFrame("Search Data | Mashina Pharmacy Information System");
        //Enter MenuBar to the Frame
        frameED.setJMenuBar(mnuBar);
        
        //Enter Menus To the MenuBar
        mnuHome = new JMenu("Home");
        mnuEnterData = new JMenu("Enter Data");
        mnuEdit = new JMenu("Edit");
        mnuSearchData = new JMenu("Search Data");
        mnuDeleteData = new JMenu("Delete Data");
        mnuHelp = new JMenu("Help");
        
        mnuBar.add(mnuHome);
        mnuBar.add(mnuEnterData);
        mnuBar.add(mnuEdit);
        mnuBar.add(mnuSearchData);
        mnuBar.add(mnuDeleteData);
        mnuBar.add(mnuHelp);
        
        
        //LABEL INITIALIZATION
        //ENTERDATA LABEL(S)
        lblMedicineName = new JLabel("Medicine Name: ");
        lblStore = new JLabel("Store: ");
        lblShelf = new JLabel("Shelf: ");
        //SEARCHDATA LABELS
        lblSearchByMedicineName = new JLabel("Search By Medicine Name: ");
        lblSearchByStore = new JLabel("Search By Store: ");
        lblSearchByShelf = new JLabel("Search By Store: ");
        
        String home = "\n\n\nWELCOME TO MASHINA PHARMACY INFORMATION SYSTEM "
                + "\nFor navigations, use the menu."
                + "\n1.Home Menu - \t Welcome Message. "
                + "\n2.Enter Data - \tFor Data Entry. "
                + "\n3.Search Data Menu - \tFor Data Retrieval"
                + "\n\n\n";
        txtHome.setForeground(Color.BLUE);
        txtHome.setText(home);
        txtHome.setEditable(false);
        txtHome.setSize(300, 200);
        
        //TEXTFIELD INITIALIZATION
        //ENTERDATA TEXTFIELD(S)
        txtMedicineName = new JTextField(15);
        //SEARCHDATA TEXTFIELD(S)
        txtSearchByMedicineName = new JTextField(15);
        txtSearchByStore = new JTextField(15);
        txtSearchByShelf = new JTextField(15);
        
        //TEXT AREA INSTANTIATION
        txtaOutputMessage = new JTextArea();
        txtaOutputMessage.setEditable(false);
        
        //BUTTON INITIALIZATION
        //ENTERDATA BUTTON
        btnSave = new JButton("Save");
        btnCancel = new JButton("Cancel");
        //SEARCHDATA BUTTON
        btnSearchByMedicineName = new JButton("Search");
        btnSearchByStore = new JButton("Search");
        btnSearchByShelf = new JButton("Search");
        
        //ComboBox Initialization
        cboStore = new JComboBox();
        cboShelf = new JComboBox();
        //SEARCHDATA COMBOBOX
        cboSearchByStore = new JComboBox();
        cboSearchByShelf = new JComboBox();
        
        //Panel Initialization
        pnlNORTH = new JPanel();
        pnlWEST = new JPanel();
        pnlCENTER = new JPanel();
        pnlEAST = new JPanel();
        pnlSOUTH = new JPanel();
        pnlHome = new JPanel();
        pnlEnterData = new JPanel();
        pnlSearchData = new JPanel();
        
        //ADD ITEMS TO THE COMBOBOX
        cboStore.addItem("Select Store: ");
        cboStore.addItem("MainStore");
        cboStore.addItem("StoreA");
        cboStore.addItem("StoreB");
        cboStore.addItem("StoreC");
        
        cboShelf.addItem("Select Shelf: ");
        cboShelf.addItem("Shelf1");
        cboShelf.addItem("Shelf2");
        cboShelf.addItem("Shelf4");
        cboShelf.addItem("Shelf5");
        
        cboSearchByStore.addItem("Select Store: ");
        cboSearchByStore.addItem("MainStore");
        cboSearchByStore.addItem("StoreA");
        cboSearchByStore.addItem("StoreB");
        cboSearchByStore.addItem("StoreC");

        cboSearchByShelf.addItem("Select Shelf: ");
        cboSearchByShelf.addItem("Shelf1");
        cboSearchByShelf.addItem("Shelf2");
        cboSearchByShelf.addItem("Shelf4");
        cboSearchByShelf.addItem("Shelf5");
        
        frameED.setLayout(new BorderLayout());
        pnlCENTER.setLayout(new FlowLayout());
        pnlSOUTH.setLayout(new FlowLayout());
        pnlEnterData.setLayout(new GridLayout(4,2));
        pnlSearchData.setLayout(new GridLayout(3,3));
        pnlHome.setLayout(new GridLayout(1,1));
        
        
        //ADD PANELS TO FRAME
        //ENTERDATA PANEL
        frameED.add(pnlNORTH, BorderLayout.NORTH);
        frameED.add(pnlWEST, BorderLayout.WEST);
        frameED.add(pnlCENTER, BorderLayout.CENTER);
        frameED.add(pnlEAST, BorderLayout.EAST);
        frameED.add(pnlSOUTH, BorderLayout.SOUTH);
        
        pnlCENTER.add(pnlEnterData);
        //SEARCHDATA PANEL

        
        //ADD COMPONENTS TO PANELS
        //HOME Components
        pnlHome.add(txtHome);
        //ENTER DATA COMPONENTS
        pnlEnterData.add(lblMedicineName);
        pnlEnterData.add(txtMedicineName);
        pnlEnterData.add(lblStore);
        pnlEnterData.add(cboStore);
        pnlEnterData.add(lblShelf);
        pnlEnterData.add(cboShelf);
        pnlSOUTH.add(btnSave);
        pnlSOUTH.add(btnCancel);
        //SEARCH DATA COMPONENTS
        pnlSearchData.add(lblSearchByMedicineName);
        pnlSearchData.add(txtSearchByMedicineName);
        pnlSearchData.add(btnSearchByMedicineName);
        pnlSearchData.add(lblSearchByStore);
        pnlSearchData.add(cboSearchByStore);
        pnlSearchData.add(btnSearchByStore);
        pnlSearchData.add(lblSearchByShelf);
        pnlSearchData.add(cboSearchByShelf);
        pnlSearchData.add(btnSearchByShelf);
        
        frameED.setVisible(true);
        frameED.setSize(400, 300);
        
        frameED.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //ADD ACTION LISTENERS
        btnSave.addActionListener(this);
        btnCancel.addActionListener(this);
        btnSearchByMedicineName.addActionListener(this);
        mnuHome.addMenuListener(this);
        mnuEnterData.addMenuListener(this);
        mnuSearchData.addMenuListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==btnSave){
            String medicineName = txtMedicineName.getText();
            String store = cboStore.getSelectedItem().toString();
            String shelf = cboShelf.getSelectedItem().toString();
            alMedicineName.add(medicineName);
            alStore.add(store);
            alShelf.add(shelf);
            try{
                    //Data saved
                Scanner scanner = new Scanner(new File("C:\\Users\\user\\Documents\\NetBeansProjects\\MashinaPhamacyInformationSystem\\src\\mashinaphamacyinformationsystem\\EnteredData.txt"));     
                File file = new File("C:\\Users\\user\\Documents\\NetBeansProjects\\MashinaPhamacyInformationSystem\\src\\mashinaphamacyinformationsystem\\EnteredData.txt");
                FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
                BufferedWriter bw = new BufferedWriter(fw);
                String enteredData = "\n"+medicineName+" "+store+" "+shelf;
                bw.write(enteredData);
                bw.close();
                while(scanner.hasNextLine()){
                    String line = scanner.nextLine();
                    System.out.println(line);
                }
            }catch(FileNotFoundException fnfe){
                fnfe.printStackTrace();
            }catch(IOException ioe){
                ioe.printStackTrace();
            }
            
            txtMedicineName.setText("");
            cboStore.setSelectedIndex(0);
            cboShelf.setSelectedIndex(0);
        }else if(e.getSource()==btnCancel){
            printData();
            txtMedicineName.setText("");
            cboStore.setSelectedIndex(0);
            cboShelf.setSelectedIndex(0);
        }else if(e.getSource()==mnuSearchData){
                    //SEARCHDATA PANEL
            frameSD.add(pnlNORTH, BorderLayout.NORTH);
            frameSD.add(pnlWEST, BorderLayout.WEST);
            frameSD.add(pnlCENTER, BorderLayout.CENTER);
            frameSD.add(pnlEAST, BorderLayout.EAST);
            frameSD.add(pnlSOUTH, BorderLayout.SOUTH);
            pnlCENTER.add(lblSearchByMedicineName);
            pnlCENTER.add(txtSearchByMedicineName);
            pnlCENTER.add(btnSearchByMedicineName);
            pnlCENTER.add(lblSearchByStore);
            pnlCENTER.add(txtSearchByStore);
            pnlCENTER.add(btnSearchByStore);
            pnlCENTER.add(lblSearchByShelf);
            pnlCENTER.add(txtSearchByStore);
            pnlCENTER.add(btnSearchByStore);
            
            frameSD.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frameSD.setSize(400, 300);
            pnlCENTER.setLayout(new GridLayout(1,6));
            frameED.setVisible(false);
            frameSD.setVisible(true);
        }else if(e.getSource()==btnSearchByMedicineName){
            if(alMedicineName.isEmpty()){
                JOptionPane.showMessageDialog(null,"No Record Found");
            }
            String medicineName = txtSearchByMedicineName.getText();
            int index = alMedicineName.indexOf(medicineName);
            if(index == -1){
                JOptionPane.showMessageDialog(null, medicineName+" Not Found");
                txtSearchByMedicineName.setText("");
            }else{
                String outPutMessage = 
                        medicineName+" Found"+"\n\n\n---MedicineInformation---\n"+
                        "\nName: \t"+medicineName+
                        "\nStore: \t"+alStore.get(index)+
                        "\nShelf: \t"+alShelf.get(index);
                JOptionPane.showMessageDialog(null, txtaOutputMessage = new JTextArea(outPutMessage));
                txtSearchByMedicineName.setText("");
            }
        }
    }
    public void printData(){
        JOptionPane.showMessageDialog(null, alMedicineName+"\n"+alStore+"\n"+alShelf);
        
    }
    @Override
    public void menuSelected(MenuEvent e){
        if(e.getSource()== mnuHome){
            JOptionPane.showMessageDialog(null, mnuHome.getText()+" Clicked");
            frameED.setTitle("Home | Mashina Pharmacy Information System");
            frameED.remove(pnlSOUTH);
            frameED.validate();
            pnlCENTER.removeAll();
            pnlCENTER.add(pnlHome);
            pnlCENTER.validate();
            pnlSOUTH.validate();
        }else if(e.getSource()== mnuEnterData){
            pnlCENTER.removeAll();
            pnlCENTER.add(pnlEnterData);
            pnlCENTER.validate();
            frameED.add(pnlSOUTH,BorderLayout.SOUTH);
            frameED.setSize(400,300);
            frameED.validate();
        }else if(e.getSource()==mnuSearchData){
            pnlCENTER.removeAll();
            pnlCENTER.add(pnlSearchData);
            pnlCENTER.validate();
            frameED.remove(pnlSOUTH);
            frameED.setSize(600,300);
            frameED.setTitle("Search Data | Mashina Pharmacy Information System");
            frameED.validate();
        }else if(e.getSource()== mnuEdit){
            frameED.setTitle("Edit | Mashina Pharmacy Information System");
            frameED.remove(pnlSOUTH);
            frameED.validate();
            pnlCENTER.removeAll();
            pnlCENTER.add(pnlHome);
            pnlCENTER.validate();
            pnlSOUTH.validate();
        }else if(e.getSource()== mnuDeleteData){
            frameED.setTitle("Delete Data | Mashina Pharmacy Information System");
            frameED.remove(pnlSOUTH);
            frameED.validate();
            pnlCENTER.removeAll();
            pnlCENTER.add(pnlHome);
            pnlCENTER.validate();
            pnlSOUTH.validate();
        }else{
            frameED.setTitle("Help | Mashina Pharmacy Information System");
            frameED.remove(pnlSOUTH);
            frameED.validate();
            pnlCENTER.removeAll();
            pnlCENTER.add(pnlHome);
            pnlCENTER.validate();
            pnlSOUTH.validate();
        }
    }
    @Override 
    public void menuDeselected(MenuEvent e){}
    @Override
    public void menuCanceled(MenuEvent e){}
    
    public static void main(String[] args){
        EnterData enterData = new EnterData();
    }
}
