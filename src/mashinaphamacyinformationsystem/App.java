/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mashinaphamacyinformationsystem;

/**
 *
 * @author user
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class App {
   JFrame frame = new JFrame("Mashina Pharmacy Information System");
   JButton btnEnterData, btnSearchData;
   public App(){
       frame.setVisible(true);
       frame.setSize(300,200);
       frame.setLayout(new FlowLayout());
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
       btnEnterData = new JButton("Enter Data");
       btnSearchData = new JButton("Search Data");
       
       frame.add(btnEnterData);
       frame.add(btnSearchData);
   }
   public static void main(String[] s){
       new App();
   }
}
